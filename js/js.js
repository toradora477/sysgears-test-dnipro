// Task 1

console.log("Test 1");

function LengthConverterMeters(valNum) {
  document.getElementById("outputMeters").innerHTML = valNum;
  document.getElementById("outputCentimeters").innerHTML = valNum / 100.00;
  document.getElementById("outputInches").innerHTML = valNum / 39.37;
  document.getElementById("outputFeet").innerHTML = valNum / 3.2808;
}
function LengthConverterCentimeters(valNum) {
  document.getElementById("outputMeters").innerHTML = valNum / 0.01;
  document.getElementById("outputCentimeters").innerHTML = valNum;
  document.getElementById("outputInches").innerHTML = valNum / 0.39370;
  document.getElementById("outputFeet").innerHTML = valNum / 0.03808;
}
function LengthConverterInches(valNum) {
  document.getElementById("outputMeters").innerHTML = valNum / 2.54;
  document.getElementById("outputCentimeters").innerHTML = valNum / 25.40;
  document.getElementById("outputInches").innerHTML = valNum;
  document.getElementById("outputFeet").innerHTML = valNum / 0.08333;
}
function LengthConverterFeet(valNum) {
  document.getElementById("outputMeters").innerHTML = valNum / 0.3048;
  document.getElementById("outputCentimeters").innerHTML = valNum / 30.48;
  document.getElementById("outputInches").innerHTML = valNum / 12;
  document.getElementById("outputFeet").innerHTML = valNum;
}

// Task 2

console.log("Test 2");

// JSON объект со списком данных (data), и условием для обработки (condition):

let myObject = [{ "user": "mike@mail.com", "rating": 20, "disabled": false },
{"user": "greg@mail.com", "rating": 14, "disabled": false},
{"user": "john@mail.com", "rating": 25, "disabled": true}]

console.log(typeof myObject)

let myJson = JSON.stringify(myObject)

console.log(typeof myJson)

let rating = JSON.parse(myJson).sort(function(a, b) {
    return a.rating - b.rating;
}); 
console.log(rating)
// Сортировка по rating

let user = JSON.parse(myJson).sort(function(a, b) {
    return ((a.user === b.user) ? 0 : ((a.user > b.user) ? 1 : -1));
});
console.log(user);
// Сортировка по user

// Task 3

console.log("Test 3");

let dot_x_a = 0;
let dot_y_a = 0;
let dot_z_a = 0;

const dot_x_b = Math.floor(Math.random() * 10);
const dot_y_b = Math.floor(Math.random() * 10);
const dot_z_b = Math.floor(Math.random() * 10);

console.log("Точка А:", dot_x_a, dot_y_a, dot_z_a);
console.log("Точка B:", dot_x_b, dot_y_b, dot_z_b);

let distance_final

function f_distance_final(x, y, z) {
  distance_final = Math.sqrt((Math.pow((dot_x_a - dot_x_b), 2) + Math.pow((dot_y_a - dot_y_b), 2) + Math.pow((dot_z_a - dot_z_b), 2)));
  console.log("Точки: ", x, y, z, "Дистанция: ", distance_final);
// The formula for calculating the distance between two points A(xa, ya, za) and B(xb, yb, zb) in space:
// AB = √(xb - xa)2 + (yb - ya)2 + (zb - za)2
}

f_distance_final(dot_x_a, dot_y_a, dot_z_a)

const distance_initial = distance_final;
console.log("Нач. Дистанция: ", distance_initial, "Кон. Дистанция: ", distance_final);

function plus_x() {
  dot_x_a = dot_x_a + 1;
}
function plus_y() {
  dot_y_a = dot_y_a + 1;
}
function plus_z() {
  dot_z_a = dot_z_a + 1;
}
function minus_x() {
  dot_x_a = dot_x_a - 1;
}
function minus_y() {
  dot_y_a = dot_y_a - 1;
}
function minus_z() {
  dot_z_a = dot_z_a - 1;
}

if (distance_final != 0) {

  for (let x = 0, d0 = distance_final + 1; x < 10; x++) {
    
    if (distance_final != 0 && d0 > distance_final ) {
      d0 = distance_final;
      console.log("Шаг x: ", x, "Прош. Дистанция: ", d0);
      
      plus_x();
      f_distance_final(dot_x_a, dot_y_a, dot_z_a)
      
    }
  }

  minus_x();
  f_distance_final(dot_x_a, dot_y_a, dot_z_a);

  for (let y = 0, d0 = distance_final; y < 10; y++) {
    
    if (distance_final != 0 && d0 >= distance_final) {
      d0 = distance_final;
      console.log("Шаг y: ", y, "Прош. Дистанция: ", d0);

      plus_y();
      f_distance_final(dot_x_a, dot_y_a, dot_z_a)
    }
  }

  minus_y();
  f_distance_final(dot_x_a, dot_y_a, dot_z_a);

  for (let z = 0, d0 = distance_final; z < 10; z++) {

    if (distance_final != 0 && d0 >= distance_final ) {
      d0 = distance_final;
      console.log("Шаг z: ", z, "Прош. Дистанция: ", d0);

      plus_z();
      f_distance_final(dot_x_a, dot_y_a, dot_z_a);
    }
  }

  f_distance_final(dot_x_a, dot_y_a, dot_z_a);

}

console.log("Точка B:", dot_x_b, dot_y_b, dot_z_b);

// Test 4

console.log("Test 4");

let data = [{
    id: 'id1',
    question: 'What is your marital status?',
    answer1: {
      text: 'Single',
      next_question: 'id2.1'
    },
    answer2: {
      text: 'Married',
      next_question: 'id2.2'
    }
  },

  {
    id: 'id2.1',
    question: 'Are you planning on getting married next year?',
    answer1: {
      text: 'Yes',
      next_question: 'id2.1.1'
    },
    answer2: {
      text: 'No',
      next_question: 'id2.1.2'
    }
  },

  {
    id: 'id2.2',
    question: 'How long have you been married?',
    answer1: {
      text: 'Less than a year',
      next_question: 'id2.2.1'
    },
    answer2: {
      text: 'More than a year',
      next_question: 'id2.2.2'
    }
  },

  {
    id: 'id2.2.2',
    question: '"Have you celebrated your one year anniversary?',
    answer1: {
      text: 'Yes',
      next_question: 'id2.2.2.1'
    },
    answer2: {
      text: 'No',
      next_question: 'id2.2.2.2'
    }
  }
];
// JSON date test 4

const tree = {};
for (const o of data) {
  tree[o.id] = [o.answer1.next_question, o.answer2.next_question];
}
console.log(tree);
// Transform data

const func = (q, h) => {
  h.push(q);
  for (const a of tree[q]) {
    if (!tree[a]) {
      console.log([...h, a]); // проблема
    } else {
      func(a, [...h]);
    }
  }
};

func(Object.keys(tree)[0], []); // Сложно перебрать в нужный выходной тип данных

let text = JSON.stringify(tree);  // Доделать нормальную ссылку. Эта ссылка на первое значение массива


function downloadAsFile(data) {
  let a = document.createElement("a");
  let file = new Blob([data], {type: 'application/json'});
  a.href = URL.createObjectURL(file);
  a.download = "date.json";
  a.click();
}

// download file json (ALL)

